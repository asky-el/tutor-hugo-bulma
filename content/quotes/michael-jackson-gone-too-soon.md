+++
type       = "post"
title      = "Michael Jackson - Gone Too Soon"
date       = 2015-05-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["pop", "90s"]
slug       = "michael-jackson-gone-too-soon"
author     = "epsi"
+++

Shiny and sparkly.
And splendidly bright.
Here one day.
Gone one night.
