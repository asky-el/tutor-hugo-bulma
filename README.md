# Hugo Bulma Test Drive

An example of Hugo site using Bulma for personal learning purpose.

This is basically a Bulma version of Hugo Tutorial.

![Hugo Bulma: Preview][hugo-bulma-preview]
  
[hugo-bulma-preview]:   https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/hugo-bulma-preview.png
